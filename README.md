jQuery UI Font selector widget
==============================

This is a font selector widget which uses the Google Web Fonts library. It was made by [Olav Andreas Lindekleiv][1], and is available on [BitBucket][2]. You can also try out a live demo [here][3].

Usage
-----

To use the widget, simply create a select element with the fonts you want to use (make sure they're available from the Google Web Fonts library first!), and enable it.

      <select id="fonts">
        <option value="Chelsea Market">Chelsea Market</option>
        <option value="Droid Serif" selected="selected">Droid Serif</option>
        <option value="Ruluko">Ruluko</option>
        <option value="Ruda">Ruda</option>
        <option value="Magra">Magra</option>
        <option value="Esteban">Esteban</option>
        <option value="Lora">Lora</option>
        <option value="Jura">Jura</option>
      </select>


To enable it, do:

      $('select#fonts').fontSelector({
        options: {
          inSpeed: 250,
          outSpeed: "slow",
        }
        fontChange: function(e, ui) {
          alert("The font is set to "+ui.font+" (was "+ui.oldFont+" before)");
        }
        styleChange: function(e, ui) {
          alert("The value of "+ui.style+" was set to "+ui.value);
        }
      });

The options are optional. Default values are 500 ms for inSpeed and 250 ms for outSpeed.


License
-------

The jQuery UI Font selector widget is available under the BSD License. Please read it (see the LICENSE file). It's only a few lines.


  [1]: http://lindekleiv.com/
  [2]: https://bitbucket.org/lindekleiv/jquery-ui-fontselector
  [3]: http://lindekleiv.bitbucket.org/fontselector/